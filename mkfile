CC=gcc
SCANBD= scan-build
CFLAGS= -D_XOPEN_SOURCE=700 -g -std=c99 -Wall -pedantic

SCANBDFLAGS= -enable-checker core -enable-checker security -enable-checker unix
SPARSEFLAGS= -Wsparse-all -gcc-base-dir `gcc --print-file-name=`
CPPCHKFLAGS= --std=posix --std=c99 --platform=unix64 --enable=all --suppress=missingIncludeSystem

%.c:V:
  $SCANBD $SCANBDFLAGS $CC $CFLAGS $target -o $stem
  sparse $SPARSEFLAGS $target
  cppcheck $CPPCHKFLAGS $target
  [ -e test_$stem ] && ./test_$stem
