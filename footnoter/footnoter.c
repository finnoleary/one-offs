#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#define HELPSTRING \
 "%s -- Processes the footnotes of a file\n" \
 "Usage:\n" \
 "\t%s infile outfile > footfile\n\n" \
 "\tThe first output given is outfile, which is a replica of outfile where\n" \
 "\teach instance of a footnote '<https://foot.note>' is replaced by how\n" \
 "\tmany footnotes have currently been encountered in the file '<n>'. In\n" \
 "\taddition, the info is written out into stdout like 'n: http://foot.note'\n"\
 "\tThis allows for further processing of the data by external programs.\n"


static char *readbuf(char *path)
{
	/* declaring here fixes a bug where we jump to _throw early and end up
	   with a bogus free()/close() */
	struct stat st;
	size_t size = 0;
	char *s = NULL;
	int fd = -1;

	/* get data about file (including size in bytes) */
	if ((stat(path, &st)) < 0) { perror(path); goto _throw; }
	
	size = st.st_size;
	if (size == 0) { printf("%s is empty!\n", path); goto _throw; }

	/* The size of a char is always one */
	s = calloc(size, 1);
	if (!s) { perror("calloc"); goto _throw; }

	fd = open(path, O_RDONLY);
	if (fd < -1) { perror(path); goto _throw; }
	if ((read(fd, s, size)) < 0) { perror("read"); goto _throw; }
	close(fd); /* There's no point checking close for errors */
	
	return s;

_throw:
	if (fd > 0) close(fd);
	if (s) free(s);
	return NULL;
}


static int cpystream(FILE *instream, FILE *outstream)
{
	size_t bufsz = 4095;
	char *buffer[4096];
	long inlen = -1;

	if (!instream || !outstream) { return 1; }

	/* Zero out the buffer */
	memset(buffer, '\0', bufsz);

	if ((inlen = ftell(instream)) < 0) { return 0; }
	rewind(instream);

	while ((ftell(instream)) < inlen &&
	       !ferror(instream) && !ferror(outstream))
	{
		size_t nbytes = fread(buffer, 1, bufsz, instream);
		fwrite(buffer, 1, nbytes, outstream);
	}
	if (!ferror(instream) && !ferror(outstream)) { return 1; }
	return 0;
}


static int isfootnote(char *s)
/* This will probably fail in some really, really weird edge case */
{
	return s && s[1] && s[2] &&
	       s[0] != ' ' && s[1] == '<' && isalpha(s[2]);
}

static ssize_t scan_footer(char *inbuf, FILE *outfile, FILE *footfile)
{
	ssize_t nfts = 0;
	size_t sz = 0;
	size_t i = 0;
	size_t j = 0;

	if (!inbuf || !outfile || !footfile) { goto _throw; }

	sz = strlen(inbuf);
	while (i < sz)
	{
		/* Seek to start of footnote */
		for (i = 0; i < sz && inbuf[i]; i++) {
			if (isfootnote(inbuf+i)) {
				i+=2; nfts++; break;
			}
		}
		if (!inbuf[i]) { goto _exit; }
		/* Get length of footnote */
		for (j = i; j < sz && inbuf[j]; j++) {
			if (inbuf[j] == '>') {
				break;
			}
		}
		if (!inbuf[j]) {
			printf("You missed a '>' in your infile.\n");
			goto _throw;
		}
		/* Write out the "%ld: %s\n" entry to footfile */
		fprintf(footfile, "%ld: ", nfts);
		fwrite(inbuf+i, 1, abs(j-i), footfile);
		fputc('\n', footfile);
		if (ferror(footfile)) { goto _throw; }

		/* Write out the data before the footnote, then write out the
		   modified footnote "<%ld>" here */
		fwrite(inbuf, 1, i-1, outfile);
		fprintf(outfile, "<%ld>", nfts);
		if (ferror(outfile)) { goto _throw; }

		/* We have to alter the base of inbuf otherwise we'll write
		   from the beginning constantly */

		inbuf += j+1;
	}
_exit:
	if (inbuf) { 
		/* Write the rest of the output */
		sz = strlen(inbuf);
		fwrite(inbuf, 1, sz, outfile);
	}
	return nfts;
		
_throw:
	return -1;
}


int main(int argc, char **argv)
{
	char *inbuf = NULL;
	FILE *outfile = NULL; FILE *footfile = NULL;

	if (argc < 3) { printf(HELPSTRING, argv[0], argv[0]); return 0; }

	if (!(inbuf = readbuf(argv[1]))) { goto _throw; }
	if (!(outfile = fopen(argv[2], "w"))) { perror(argv[2]); goto _throw; }
	if (!(footfile = tmpfile())) { perror("tmpfile"); goto _throw; }

	if ((scan_footer(inbuf, outfile, footfile)) < 0) { goto _throw; }

	if ((cpystream(footfile, stdout)) < 1) {
		perror("cpystream"); goto _throw;
	}

	if (inbuf) { free(inbuf); }
	if (outfile) { fclose(outfile); }
	if (footfile) { fclose(footfile); }

	return 0;
_throw:
	if (inbuf) { free(inbuf); }
	if (outfile) { fclose(outfile); }
	if (footfile) { fclose(footfile); }
	return 1;
}
