#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define uchar unsigned char
#define cmp(a,b) ((strcmp((a),(b))) == 0)

enum {
	E_BADCHAR,
	E_COMMENT,
	E_NEWLINE,
	E_REPLICATE,
	E_IGNORE,
	E_NUM
};
uchar classify[256] = { 0 };
enum {
	S_END,
	S_ERROR,

	S_FIRST,
	S_NEWLINE,
	S_GENERAL,
	S_SKIP,
	S_DISCARD,
	S_NUM
};
uchar state[S_NUM-2][E_NUM];

void fill_state(void)
{
	state[S_FIRST][E_BADCHAR] = S_ERROR;
	state[S_FIRST][E_COMMENT] = S_DISCARD;
	state[S_FIRST][E_NEWLINE] = S_GENERAL;
	state[S_FIRST][E_REPLICATE] = S_GENERAL;
	state[S_FIRST][E_IGNORE] = S_SKIP;

	state[S_NEWLINE][E_BADCHAR] = S_ERROR;
	state[S_NEWLINE][E_COMMENT] = S_DISCARD;
	state[S_NEWLINE][E_NEWLINE] = S_NEWLINE;
	state[S_NEWLINE][E_REPLICATE] = S_GENERAL;
	state[S_NEWLINE][E_IGNORE] = S_SKIP;

	state[S_GENERAL][E_BADCHAR] = S_ERROR;
	state[S_GENERAL][E_COMMENT] = S_GENERAL;
	state[S_GENERAL][E_NEWLINE] = S_NEWLINE;
	state[S_GENERAL][E_REPLICATE] = S_GENERAL;
	state[S_GENERAL][E_IGNORE] = S_SKIP;;

	state[S_SKIP][E_BADCHAR] = S_ERROR;
	state[S_SKIP][E_COMMENT] = S_GENERAL;
	state[S_SKIP][E_NEWLINE] = S_NEWLINE;
	state[S_SKIP][E_REPLICATE] = S_GENERAL;
	state[S_SKIP][E_IGNORE] = S_SKIP;;

	state[S_DISCARD][E_BADCHAR] = ;
	state[S_DISCARD][E_COMMENT] = ;
	state[S_DISCARD][E_NEWLINE] = ;
	state[S_DISCARD][E_REPLICATE] - ;
	state[S_DISCARD][E_IGNORE] = S_SKIP;;


