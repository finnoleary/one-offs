/* replacer.c V2.2.0
   gcc -std=c99 -Wall -Wextra -pedantic replacer.c -o replacer  
   Replaces a multiple strings in a file with the contents of other files.
   More functionality than the original, but also more complex. 2.5x more LoC.
   Very fast. -O3 didn't really make a difference afaict.

   MIT License; 2018 Finn Alexander O'Leary <finnoleary@inventati.org>
 */

#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#define HELPSTR \
	"%s [-r <selector>:<replacefile> ...] [-f <infile>] [-o <outfile>]\n" \
	"OR\n" \
	"%s [-r <selector>:<replacefile> ...] <infile> > <outfile>]\n"
	
struct rep_t {
	char *selector;
	char *repbuf;
};

static struct rep_t *replacements = NULL;
static size_t replacements_size = 0;

static char *_strdup(char *s)
{
	return s ? strdup(s) : NULL;
}


static char *readbuf(char *path)
{
	/* declaring here fixes a bug where we jump to _throw early and end up
	   with a bogus free()/close() */
	char *s = NULL;
	int fd = -1;
	size_t size = 0;

	struct stat st;
	/* get data about file (including size in bytes) */
	if ((stat(path, &st)) < 0) { perror(path); goto _throw; }
	
	size = st.st_size;
	if (size == 0) { printf("%s is empty.\n", path); goto _throw; }

	/* The size of a char is always one */
	s = calloc(size, 1);
	if (!s) { perror("calloc"); goto _throw; }

	fd = open(path, O_RDONLY);
	if (fd < 0) { perror(path); goto _throw; }
	if ((read(fd, s, size)) < 0) { perror("read"); goto _throw; }
	close(fd); /* There's no point checking close for errors */
	
	return s;

_throw:
	if (fd > 0) { close(fd); }
	if (s) free(s);
	return NULL;
}


static int writebuf(char *path, char *outbuf)
{
	int fd;
	char *errstr = NULL;
	if (!path) {
		if ((write(STDOUT_FILENO, outbuf, strlen(outbuf))) < 0) {
			perror("error writing output to stdout"); return 0;
		}
	}
	else {
		if ((fd = open(path, O_CREAT | O_WRONLY, 0666)) < 0) { 
			errstr = "error opening file for output"; goto _throw;
		}
		if ((write(fd, outbuf, strlen(outbuf))) < 0) {
			errstr = "error writing output to file"; goto _throw;
			return 0;
		}
		close(fd);
	}
	return 1;
_throw:
	if (fd > 0) { close(fd); }
	perror(errstr);
	return 0;
}


static size_t occurences(char *srcbuf, char *selector)
{
	size_t i = 0;
	size_t sz = strlen(selector);
	char *pt = srcbuf;
	while ((pt = strstr(pt, selector)))
		i++, pt += sz;

	return i;
}


static char *replace(char *selector, char *srcbuf, char *repbuf, char *outbuf)
{
	char *pos;
	size_t prelen, postlen, reqlen, sellen;
	size_t replen = strlen(repbuf);

	/* find the selector in the source file buffer */
	pos = strstr(srcbuf, selector);
	if (!pos) { printf("Does not contain '%s'\n", selector); return NULL; }

	/* Find out how many characters between the start and the selector */
	/* Advance pos past the selector and find out how long until file end */
	prelen = (pos - srcbuf);
	pos += (sellen = strlen(selector));
	postlen = strlen(pos);

	/* We need to calculate total size + nul and ensure the outbuf fits */
	reqlen = prelen + replen + postlen + 1;
	outbuf = realloc(outbuf, reqlen);
	if (!outbuf) { perror("Cannot alloc new output buf"); return NULL; }

	/* Write out the data */
	memcpy(outbuf, srcbuf, prelen);
	memcpy(outbuf+prelen, repbuf, replen);
	memcpy(outbuf+prelen+replen, pos, postlen);
	outbuf[prelen+replen+postlen] = '\0';

	return outbuf;
}


static int splitarg(char *s, struct rep_t *rep)
{
	char *rest = NULL;
	if (!s) { return 0; }
	do {
		if (!(rest = strchr(s, ':'))) { return 0; }
	} while (rest[0] == '\\');

	rest[0] = '\0'; rest++; 

	if (!(rep->selector = _strdup(s))) { return 0; }
	if (!(rep->repbuf = readbuf(rest))) { return 0; }
	return 1;
}


static void free_replacements(void)
{
	size_t i = 0;
	if (!replacements) return;

	for (i = 0; i < replacements_size; i++) {
		free(replacements[i].selector);
		free(replacements[i].repbuf);
	}
	free(replacements);
}


static int opts(char **argv, char **inname, char **outname)
/* 1 if success
   0 if allocation failure
  -1 if badarg / arg missing
*/
{
	size_t i, nreps, repix;
	/* First we want to know how many replacements we're being asked for */
	for (i = 0, nreps = 0; argv[i]; i++) {
		if ((strcmp("-r", argv[i])) == 0)
			nreps++, i++; /* i++ to skip over argument */
	}
	
	/* Next we exit if we're not asked to do any, else allocate rep_t's */
	if (nreps == 0) { return -1; }
	
	if (!(replacements = calloc(nreps, sizeof(struct rep_t)))) { return 0; }
	replacements_size = nreps;
	
	for (i = 1, repix = 0; argv[i]; i++) {
		if ((strcmp("-o", argv[i])) == 0) {
			if (!argv[++i] || strlen(argv[i]) == 0){ goto _badarg; }
			*outname = argv[i];
		}
		else if ((strcmp("-f", argv[i])) == 0) {
			if (!argv[++i] || strlen(argv[i]) == 0){ goto _badarg; }
			if ((strcmp("-", argv[i])) == 0) { goto _badarg; }
			*inname = argv[i];
		}
		else if ((strcmp("-r", argv[i])) == 0) {
			if (!argv[++i] || strlen(argv[i]) == 0){ goto _badarg; }
			if (!splitarg(argv[i], &replacements[repix++])) {
				goto _badarg;
			}
		}
		else if ((strcmp("-", argv[i])) == 0) {
			goto _badarg;
		}
		else {
			*inname = argv[i];
		}
	}
	return 1;

_badarg:
	if (replacements) { free(replacements); }
	return -1;
}


int main(int argc, char **argv)
{
	/* see line 11 for explanation */
	size_t i;
	size_t nreps = 0;
	struct rep_t *rep = NULL;
	char *srcbuf = NULL; char *outbuf = NULL;
	char *infile = NULL; char *outfile = NULL;

	if (argc < 2 || opts(argv, &infile, &outfile) < 1) {
		printf("Bad or insufficient arguments\n");
		printf(HELPSTR, argv[0], argv[0]);
		exit(1);
		/* goto _throw; */
	}

	srcbuf = readbuf(infile);
	if (!srcbuf) { goto _throw; }
	
	for (i = 0; i < replacements_size; i++)
	{
		rep = &replacements[i];
		nreps = occurences(srcbuf, rep->selector);
		while (nreps-- > 0) {
			outbuf = replace(rep->selector,
			                 srcbuf, rep->repbuf, outbuf);
			free(srcbuf); srcbuf = _strdup(outbuf);
			if (!outbuf || !srcbuf) { goto _throw; }
		}
	}

	if (!writebuf(outfile, outbuf)) { goto _throw; }

	/* close bufs */
	free(srcbuf), srcbuf = NULL;
	free(outbuf), outbuf = NULL;
	free_replacements();

	return 0;

_throw:
	if (srcbuf) { free(srcbuf); }
	if (outbuf) { free(outbuf); }
	free_replacements();
	return 1;
}

