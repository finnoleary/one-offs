#define _XOPEN_SOURCE 700
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

static char *readbuf(char *path)
{
	/* declaring here fixes a bug where we jump to _throw early and end up
	   with a bogus free()/close() */
	char *s = NULL;
	int fd = -1;

	struct stat st;
	/* get data about file (including size in bytes) */
	if ((stat(path, &st)) < 0) { perror(path); goto _throw; }
	
	size_t size = st.st_size;
	if (size == 0) { printf("%s is empty!\n", path); goto _throw; }

	/* The size of a char is always one */
	s = calloc(size, 1);
	if (!s) { perror("calloc"); goto _throw; }

	fd = open(path, O_RDONLY);
	if (fd < -1) { perror(path); goto _throw; }
	if ((read(fd, s, size)) < 0) { perror("read"); goto _throw; }
	close(fd); /* There's no point checking close for errors */
	
	return s;

_throw:
	if (fd > 0) close(fd);
	if (s) free(s);
	return NULL;
}

int main(int argc, char **argv)
{
	/* see line 11 for explanation */
	char *sourcebuf = NULL;
	char *repbuf = NULL;
	if (argc < 4) { /* zero counts as one item */
		printf("%s <selector> <src> <replacement>\n", argv[0]);
		goto _throw;
	}

	char *selector = argv[1];
	char *source = argv[2];
	char *replacement = argv[3];

	/* read in the source file */
	sourcebuf = readbuf(source);
	if (!sourcebuf) goto _throw;

	/* find the selector in the source file buffer */
	char *pos = strstr(sourcebuf, selector);
	if (!pos) { printf("Does not contain '%s'\n", selector); goto _throw; }

	/* Find out how many characters between the start and the selector */
	size_t length = (pos - sourcebuf);

	/* write out the first file up to the selector */
	if (length > 0 && (write(STDOUT_FILENO, sourcebuf, length)) < 0) {
		perror("error writing file to stdout"); goto _throw;
	}
	
	/* read in the replacement file */
	repbuf = readbuf(replacement);
	if (!repbuf) goto _throw;

	/* get the length of the replacement file */
	size_t replen = strlen(replacement);

	/* write out the replacement file */
	if (replen > 0 && (write(STDOUT_FILENO, repbuf, replen)) < 0) {
		perror("error writing replacement to stdout"); goto _throw;
	}

	/* close repbuf */
	free(repbuf); repbuf = NULL;
	/* ^ Free does not guarantee NULL; do this to avoid double free */
	
	/* advance pos past the selector */
	pos += strlen(selector);
	/* How long is the rest of the file? */
	size_t restlen = strlen(pos);

	if (restlen > 0 && (write(STDOUT_FILENO, pos, restlen)) < 0) {
		perror("error writing rest of source to stdout"); goto _throw;
	}

	/* close sourcebuf */
	free(sourcebuf); sourcebuf = NULL;

	return 0;

_throw:
	if (sourcebuf) { free(sourcebuf); }
	if (repbuf) { free(repbuf); }
	return 1;
}
